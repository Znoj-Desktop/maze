#include "like_paralel.h"

#define lp_stackSize 16777216
int lp_stack[4][lp_stackSize];

bool lp_pop(int &x, int &y, int h, int lp_stack[lp_stackSize], int &lp_stackPointer)
{
	if (lp_stackPointer > 0)
	{
		int p = lp_stack[lp_stackPointer];
		x = p / h;
		y = p % h;
		lp_stackPointer--;
		return 1;
	}
	else
	{
		return 0;
	}
}

bool lp_push(int x, int y, int h, int lp_stack[lp_stackSize], int &lp_stackPointer)
{
	if (lp_stackPointer < lp_stackSize - 1)
	{
		lp_stackPointer++;
		lp_stack[lp_stackPointer] = h * x + y;
		return 1;
	}
	else
	{
		return 0;
	}
}

void lp_emptyStack(int h, int lp_stack[lp_stackSize], int &lp_stackPointer)
{
	//int x, y;
	//while (lp_pop(x, y, h, lp_stack, lp_stackPointer));
	lp_stackPointer = 0;
}




//globalni promenne vyuzivane v alg
int lp_width, lp_height;
bool lp_pole[MAX][MAX];
int lp_obarveno[MAX][MAX];
int global_lp_barva = 2;	//cislo 1 znamena, ze zde byla barva

const int max_pole = (MAX / 3)*(MAX / 3);
int lp_barvy_cykly[max_pole][2];
int lp_barvy_cykly_pointer = 0;

//lehce modifikovane seminkove vyplnovani
//Otestuj zda bod ji� nebyl vypln�n nebo nele�� na hranici.
//Pokud ano skon��, pokud ne vypl� tento bod a ur�i v�echny sousedn� body.
//Pro v�echny sousedn� body znovu pou�ij tento algoritmus.
void vypis_lp();
int seminkove_like_paralel(int x, int y, int hl, int hh, int lp_stack[lp_stackSize], int &lp_stackPointer, int &lp_barva){
	//pokud jsem mimo hranice, pak vracim -1,
	//ktera znaci, ze se nejedna o kruznici
	if (x >= 3 * lp_width || x < 0 || y < hl || y >= hh){
		return -1;
	}
	//pokud narazim na jiz obarveny uzel nebo okraj kruznice, tak vracim 0
	else if ((lp_obarveno[y][x] > 0) || lp_pole[y][x]){
		return 0;
	}
	else{
		int vpravo = 0;
		int vlevo = 0;
		int nahore = 0;
		int dole = 0;
		int pocet = 0;
		bool cyklus = 1;
		//?? fakt?
		lp_emptyStack(3 * lp_height, lp_stack, lp_stackPointer);
		if (!lp_push(x, y, 3 * lp_height, lp_stack, lp_stackPointer)){
			return -2;
		}
		while (lp_pop(x, y, 3 * lp_height, lp_stack, lp_stackPointer)){
			if (lp_obarveno[y][x] > 0){
				continue;
			}
			pocet++;
			lp_obarveno[y][x] = lp_barva;
			//vypis_lp();
			//jdu se podivat do vsech smeru
			if ((x + 1) >= 3 * lp_width){
				vpravo = -1;
				cyklus = 0;
			}
			else if ((lp_obarveno[y][x + 1] == 0) && !lp_pole[y][x + 1]){
				if (!lp_push(x + 1, y, 3 * lp_height, lp_stack, lp_stackPointer)) {
					return -2;
				}
				//vpravo += 1;
			}

			if ((x - 1) < 0){
				vlevo = -1;
				cyklus = 0;
			}
			else if (lp_obarveno[y][x - 1] == 0 && !lp_pole[y][x - 1]){
				if (!lp_push(x - 1, y, 3 * lp_height, lp_stack, lp_stackPointer)){
					return -2;
				}
				//vlevo += 1;
			}

			if ((y + 1) >= 3 * lp_height){
				nahore = -1;
				cyklus = 0;
			}
			//mimo aktualni vysec
			else if ((y + 1) >= hh){
				;
			}
			else if (lp_obarveno[y + 1][x] == 0 && !lp_pole[y + 1][x]){
				if (!lp_push(x, y + 1, 3 * lp_height, lp_stack, lp_stackPointer)){
					return -2;
				}
				//nahore += 1;
			}
			if ((y - 1) < 0){
				dole = -1;
				cyklus = 0;
			}
			//mimo aktualni vysec
			if ((y - 1) < hl){
				;
			}
			else if (lp_obarveno[y - 1][x] == 0 && !lp_pole[y - 1][x]){
				if (!lp_push(x, y - 1, 3 * lp_height, lp_stack, lp_stackPointer)){
					return -2;
				}
				//dole += 1;
			}
		} //konec while

		/*
		
		vypis_lp();

		*/


		//pokud nejsem mimo lp_pole, pricitam
		if (cyklus && vpravo >= 0 && vlevo >= 0 && nahore >= 0 && dole >= 0){
			//return (vpravo + vlevo + nahore + dole + 1);
			return pocet + 1;
		}
		else{
			//znacim ze se nejedna o kruznici
			return -1;
			//return pocet + 1;
		}
	}
} //konec seminkoveho vyplnovani

/*
int je_v_poli_lp(int prvek, int pole[][2], int min, int max){
	if (prvek == 0){
		return -1;
	}
	int index = ((max - min) / 2) + min;
	if (pole[index][0] == prvek){
		return index;
	}
	else if (index == min){
		if (pole[max][0] == prvek){
			return index;
		}
		return -1;
	}
	else if (prvek < pole[index][0]){
		return je_v_poli_lp(prvek, pole, min, index);
	}
	else{
		return je_v_poli_lp(prvek, pole, index, max);
	}
	return -1;
}
*/
//aby to sedelo s paralelnim
int je_v_poli_lp(int prvek, int pole[][2], int min, int max){
	//printf("prvek: %d\n", prvek);
	for (int i = min; i <= max; i++){
		//printf("%d\n", pole[i][0]);
		if (prvek == pole[i][0]){
			return i;
		}
	}
	return -1;
}

void zpracuj_lp(int co, int na_co){
	for (int i = 0; i < 3 * lp_height; i++){
		for (int j = 0; j < 3 * lp_width; j++){
			if (lp_obarveno[i][j] == co){
				lp_obarveno[i][j] = na_co;
			}
		}
	}
}

void vypis_lp(){
	for (int l = 0; l < 3 * lp_width; l++){
		printf("%d", l % 10);
	}
	printf("     X");
	for (int l = 0; l < 3 * lp_width; l++){
		printf("%d", l % 10);
	}
	printf("\n");

	for (int k = 3 * lp_height - 1; k >= 0; k--){
		for (int l = 0; l < 3 * lp_width; l++){
			if (!lp_pole[k][l]){
				printf("  ");
			}
			else{
				printf("##");
			}
		}

		printf("     ");
		printf("%d", k % 10);

		for (int l = 0; l < 3 * lp_width; l++){
			if (lp_obarveno[k][l] > 0){
				printf("%2d", lp_obarveno[k][l]);
				//printf("*");
			}
			else{
				printf("  ");
			}
		}
		printf("\n");
	}
	printf("\n");
}

void vypis_lp_final(){
	for (int l = 0; l < 3 * lp_width; l++){
		printf("%d", l % 10);
	}
	printf("     X");
	for (int l = 0; l < 3 * lp_width; l++){
		printf("%d", l % 10);
	}
	printf("\n");

	for (int k = 3 * lp_height - 1; k >= 0; k--){
		for (int l = 0; l < 3 * lp_width; l++){
			if (!lp_pole[k][l]){
				printf(" ");
			}
			else{
				printf("#");
			}
		}

		printf("     ");
		printf("%d", k % 10);

		for (int l = 0; l < 3 * lp_width; l++){
			if (je_v_poli_lp(lp_obarveno[k][l], lp_barvy_cykly, 0, lp_barvy_cykly_pointer - 1) >= 0){
				//printf("%d", (lp_obarveno[k][l]));
				//printf("*");
				printf(" ");
			}
			else{
				lp_obarveno[k][l] = -1;
				printf("#");
				//printf(" ");
			}
		}
		printf("\n");
	}
}

int main_like_paralel(){
	int lp_stackPointer = 0;
	int poradi = 1;
	int nejdelsi;
	int kruznic;
	char znak;
	int i;
	int j;
	clock_t start, finish;

	//nekonecny cyklus pro nacitani vstupu
	while (scanf_s("%i %i", &lp_width, &lp_height) > 0){
		//zde je konec programu--------------------------
		if (lp_width == 0 && lp_height == 0){
			return 0;
		}

		//nulovani
		nejdelsi = 0;
		kruznic = 0;

		//nulovani poli - jen pro jistotu...
		for (i = 0; i < MAX; i++){
			for (j = 0; j < MAX; j++){
				lp_pole[i][j] = 0;
				lp_obarveno[i][j] = 0;
			}
		}

		//ulozeni vstupu do lp_pole
		for (i = 0; i < lp_height; i++){
			for (j = 0; j < lp_width; j++){
				znak = getchar();
				//preskoceni nepatricnych znaku
				while (znak != '/' && znak != '\\'){
					znak = getchar();
				}
				//zakresleni znaku '/' do ctverce 3x3
				if (znak == '/'){
					lp_pole[3 * i + 0][3 * j + 2] = true;
					lp_pole[3 * i + 2][3 * j + 0] = true;
				}
				//zakresleni znaku '\' do ctverce 3x3
				else{
					lp_pole[3 * i + 0][3 * j + 0] = true;
					lp_pole[3 * i + 2][3 * j + 2] = true;
				}
				//pro '/' i '\' je prostredni ctverec stejny
				lp_pole[3 * i + 1][3 * j + 1] = true;
			}
		}
		int velikost_kruznice;
		start = clock();
		//----------------------------------------------------------------
		//[pocet vlaken][min_h, max_h]
		const int pocet_vlaken = 4;
		int rozmery[pocet_vlaken][2];

		rozmery[0][0] = 0;
		rozmery[0][1] = lp_height / pocet_vlaken;
		for (int i = 1; i < 4; i++){
			rozmery[i][0] = rozmery[i - 1][1];
			rozmery[i][1] = (i + 1) * rozmery[0][1];
		}
		rozmery[3][1] = lp_height;

		for (int r = 0; r < pocet_vlaken; r++){
			//lp_barva = omlp_get_thread_num() + 1;
			global_lp_barva = r*(16777216 / pocet_vlaken) + 2;
			for (int i = 3 * rozmery[r][0]; i < 3 * rozmery[r][1]; i++){
				for (int j = 0; j < 3 * lp_width; j++){
					velikost_kruznice = seminkove_like_paralel(j, i, 3 * rozmery[r][0], 3 * rozmery[r][1], lp_stack[r], lp_stackPointer, global_lp_barva);
					
					if (velikost_kruznice == 0){
						continue;
					}
					else{
						global_lp_barva++;
					}
					if (velikost_kruznice == -2){
						printf("\npodteceni nebo preteceni zasobniku.\n");
						continue;
					}
					if (velikost_kruznice > 0){
						lp_barvy_cykly[lp_barvy_cykly_pointer][0] = global_lp_barva-1;
						lp_barvy_cykly[lp_barvy_cykly_pointer][1] = velikost_kruznice / 3;
						lp_barvy_cykly_pointer++;
						kruznic++;
					}
				}
			}
		}

		double middle = ((double)(clock() - start)) / CLOCKS_PER_SEC;
		//----------------------------------------------------------------
		//postprocessing
		//printf("POST\n");
		for (int r = 0; r < (pocet_vlaken - 1); r++){
			for (int j = 0; j < 3 * lp_width; j++){
				//pokud jsou cisla na hranici ruzna a znaci barvy { hranice: 3 * rozmery[r][1], (3 * rozmery[r][1]) + 1 }
				if ((lp_obarveno[(3 * rozmery[r][1]) - 1][j] > 0) && (lp_obarveno[(3 * rozmery[r][1])][j] > 0)){
					if (((lp_obarveno[(3 * rozmery[r][1]) - 1][j]) != (lp_obarveno[(3 * rozmery[r][1])][j]))){
						//jedna se o cyklus
						int prvek1 = je_v_poli_lp(lp_obarveno[(3 * rozmery[r][1]) - 1][j], lp_barvy_cykly, 0, lp_barvy_cykly_pointer - 1);
						int prvek2 = je_v_poli_lp(lp_obarveno[(3 * rozmery[r][1])][j], lp_barvy_cykly, 0, lp_barvy_cykly_pointer - 1);
						if ((prvek1 >= 0) && (prvek2 >= 0)){
							//co se zobrazi kam
							zpracuj_lp(lp_obarveno[(3 * rozmery[r][1])][j], lp_obarveno[(3 * rozmery[r][1]) - 1][j]);
							lp_barvy_cykly[prvek1][1] += lp_barvy_cykly[prvek2][1]; 
							lp_barvy_cykly[prvek2][1] = 0;
							kruznic--;
						}
						//nejaka cast se dotyka okraje - nejde o cyklus
						else{
							if (prvek1 >= 0){
								lp_barvy_cykly[prvek1][1] = 0;
								kruznic--;
							}
							else if (prvek2 >= 0){
								lp_barvy_cykly[prvek2][1] = 0;
								kruznic--;
							}
							zpracuj_lp(lp_obarveno[(3 * rozmery[r][1]) - 1][j], 1);
							zpracuj_lp(lp_obarveno[(3 * rozmery[r][1])][j], 1);
						}
					}
				}


				//vypis_lp();

				
			}
		}
		
		//-----------------------------------------------------------------
		finish = clock();
		//finalni vypis_lp
		printf("Maze #%d:\n", poradi++);

		if (kruznic == 0){
			printf("There are no cycles.\n\n");
		}
		else{
			nejdelsi = lp_barvy_cykly[0][1];
			for (int i = 1; i < lp_barvy_cykly_pointer; i++){
				if (lp_barvy_cykly[i][1] > nejdelsi){
					nejdelsi = lp_barvy_cykly[i][1];
				}
			}
			printf("%d Cycles; the longest has length %d.\n\n", kruznic, nejdelsi);
		}
		printf("It middle: %fs\n", middle);
		printf("It took %fs\n", ((double)(finish - start)) / CLOCKS_PER_SEC);

		/*
		for (int i = 0; i < lp_barvy_cykly_pointer; i++){
			printf("%d - %d  ", i, lp_barvy_cykly[i]);
		}
		printf("\n\n");*/
		//----------------------------------------------------------------

		//vypis_lp_final();

		//----------------------------------------------------------------

	}
	return 0;
}