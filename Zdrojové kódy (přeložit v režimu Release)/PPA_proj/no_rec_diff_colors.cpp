#include "no_rec_diff_colors.h"

#define c_stackSize 16777216
int c_stack[c_stackSize];
int c_stackPointer;

bool c_pop(int &x, int &y, int h)
{
	if (c_stackPointer > 0)
	{
		int p = c_stack[c_stackPointer];
		x = p / h;
		y = p % h;
		c_stackPointer--;
		return 1;
	}
	else
	{
		return 0;
	}
}

bool c_push(int x, int y, int h)
{
	if (c_stackPointer < c_stackSize - 1)
	{
		c_stackPointer++;
		c_stack[c_stackPointer] = h * x + y;
		return 1;
	}
	else
	{
		return 0;
	}
}

void c_emptyStack(int h)
{
	int x, y;
	while (c_pop(x, y, h));
}




//globalni promenne vyuzivane v alg
int c_width, c_height;
bool c_pole[MAX][MAX];
int c_obarveno[MAX][MAX];
int barva = 1;


const int max_pole = (MAX / 3)*(MAX / 3);
int c_barvy_cykly[max_pole];
int c_barvy_cykly_pointer = 0;
//lehce modifikovane seminkove vyplnovani
//Otestuj zda bod ji� nebyl vypln�n nebo nele�� na hranici.
//Pokud ano skon��, pokud ne vypl� tento bod a ur�i v�echny sousedn� body.
//Pro v�echny sousedn� body znovu pou�ij tento algoritmus.
int seminkove_no_diff(int x, int y){
	//pokud jsem mimo hranice, pak vracim -1,
	//ktera znaci, ze se nejedna o kruznici
	if (x >= 3 * c_width || x < 0 || y < 0 || y >= 3 * c_height){
		return -1;
	}
	//pokud narazim na jiz obarveny uzel nebo okraj kruznice, tak vracim 0
	else if ((c_obarveno[y][x] > 0) || c_pole[y][x]){
		return 0;
	}
	else{
		//menim barvu
		barva++;
		int vpravo = 0;
		int vlevo = 0;
		int nahore = 0;
		int dole = 0;
		int pocet = 0;
		bool cyklus = 1;
		//?? fakt?
		c_emptyStack(3 * c_height);
		if (!c_push(x, y, 3 * c_height)){
			return -2;
		}
		while (c_pop(x, y, 3 * c_height)){
			if (c_obarveno[y][x] > 0){
				continue;
			}
			pocet++;
			c_obarveno[y][x] = barva;
			
			/*
			for (int l = 0; l < 3 * c_width; l++){
				printf("%d", l % 10);
			}
			printf("     X");
			for (int l = 0; l < 3 * c_width; l++){
				printf("%d", l % 10);
			}
			printf("\n");
			
			for (int k = 3 * c_height - 1; k >= 0; k--){
				for (int l = 0; l < 3 * c_width; l++){
					if (!c_pole[k][l]){
						printf("#");
					}
					else{
						printf(" ");
					}
				}

				printf("     ");
				printf("%d", k % 10);

				for (int l = 0; l < 3 * c_width; l++){
					if (c_obarveno[k][l] > 0){
						printf("%d", c_obarveno[k][l]); 
					}
					else{
						printf(" ");
					}
				}
				printf("\n");
			}
			printf("\n");
			*/
			//jdu se podivat do vsech smeru
			if ((x + 1) >= 3 * c_width){
				vpravo = -1;
				cyklus = 0;
			}
			else if ((c_obarveno[y][x + 1] == 0) && !c_pole[y][x + 1]){
				if (!c_push(x + 1, y, 3 * c_height)) {
					return -2;
				}
				//vpravo += 1;
			}

			if ((x - 1) < 0){
				vlevo = -1;
				cyklus = 0;
			}
			else if (c_obarveno[y][x - 1] == 0 && !c_pole[y][x - 1]){
				if (!c_push(x - 1, y, 3 * c_height)){
					return -2;
				}
				//vlevo += 1;
			}

			if ((y + 1) >= 3 * c_height){
				nahore = -1;
				cyklus = 0;
			}
			else if (c_obarveno[y + 1][x] == 0 && !c_pole[y + 1][x]){
				if (!c_push(x, y + 1, 3 * c_height)){
					return -2;
				}
				//nahore += 1;
			}

			if ((y - 1) < 0){
				dole = -1;
				cyklus = 0;
			}
			else if (c_obarveno[y - 1][x] == 0 && !c_pole[y - 1][x]){
				if (!c_push(x, y - 1, 3 * c_height)){
					return -2;
				}
				//dole += 1;
			}
		} //konec while
		//pokud nejsem mimo c_pole, pricitam
		if (cyklus && vpravo >= 0 && vlevo >= 0 && nahore >= 0 && dole >= 0){
			//return (vpravo + vlevo + nahore + dole + 1);
			return pocet + 1;
		}
		else{
			//znacim ze se nejedna o kruznici
			return -1;
		}
	}
} //konec seminkoveho vyplnovani


bool je_v_poli(int prvek, int pole[], int min, int max){
	int index = ((max - min) / 2) + min;
	if (prvek == 0){
		return false;
	}
	if (pole[index] == prvek){
		return true;
	}
	else if(index == min){
		if (pole[max] == prvek){
			return true;
		}
		return false;
	}
	else if (prvek < pole[index]){
		return je_v_poli(prvek, pole, min, index);
	}
	else{
		return je_v_poli(prvek, pole, index, max);
	}
	return false;
}

int main_no_rec_diff_colors(){
	int poradi = 1;
	int nejdelsi;
	int kruznic;
	char znak;
	int i;
	int j;
	clock_t start, finish;

	//nekonecny cyklus pro nacitani vstupu
	while (scanf_s("%i %i", &c_width, &c_height) > 0){
		//zde je konec programu--------------------------
		if (c_width == 0 && c_height == 0){
			return 0;
		}

		//nulovani
		nejdelsi = 0;
		kruznic = 0;

		//nulovani poli - jen pro jistotu...
		for (i = 0; i < MAX; i++){
			for (j = 0; j < MAX; j++){
				c_pole[i][j] = 0;
				c_obarveno[i][j] = 0;
			}
		}

		//ulozeni vstupu do c_pole
		for (i = 0; i < c_height; i++){
			for (j = 0; j < c_width; j++){
				znak = getchar();
				//preskoceni nepatricnych znaku
				while (znak != '/' && znak != '\\'){
					znak = getchar();
				}
				//zakresleni znaku '/' do ctverce 3x3
				if (znak == '/'){
					c_pole[3 * i + 0][3 * j + 2] = true;
					c_pole[3 * i + 2][3 * j + 0] = true;
				}
				//zakresleni znaku '\' do ctverce 3x3
				else{
					c_pole[3 * i + 0][3 * j + 0] = true;
					c_pole[3 * i + 2][3 * j + 2] = true;
				}
				//pro '/' i '\' je prostredni ctverec stejny
				c_pole[3 * i + 1][3 * j + 1] = true;
			}
		}
		int velikost_kruznice;
		start = clock();

		//pro kazdy znak zavolej funkci seminkove_no
		for (i = 0; i < 3 * c_height; i++){
			for (j = 0; j < 3 * c_width; j++){
				velikost_kruznice = seminkove_no_diff(j, i);
				if (velikost_kruznice == 0){
					continue;
				}
				if (velikost_kruznice == -2){
					printf("\npodteceni nebo preteceni zasobniku.\n");
					break;
				}
				//je potreba velikost vydelit 3
				velikost_kruznice /= 3;
				if (velikost_kruznice > 0){
					c_barvy_cykly[c_barvy_cykly_pointer] = barva;
					c_barvy_cykly_pointer++;
					if (velikost_kruznice > nejdelsi){
						nejdelsi = velikost_kruznice;
					}
					kruznic++;
				}
			}
		}
		
		//-----------------------------------------------------------------
		finish = clock();
		//finalni vypis
		printf("Maze #%d:\n", poradi++);

		if (kruznic == 0){
			printf("There are no cycles.\n\n");
		}
		else{
			printf("%d Cycles; the longest has length %d.\n\n", kruznic, nejdelsi);
		}
		printf("It took %fs\n", ((double)(finish - start)) / CLOCKS_PER_SEC);
		/*
		for (int i = 0; i < c_barvy_cykly_pointer; i++){
			printf("%d - %d  ", i, c_barvy_cykly[i]);
		}
		printf("\n\n");
		*/

		/*
		for (int l = 0; l < 3 * c_width; l++){
			printf("%d", l % 10);
		}
		printf("     X");
		for (int l = 0; l < 3 * c_width; l++){
			printf("%d", l % 10);
		}
		printf("\n");

		for (int k = 3 * c_height - 1; k >= 0; k--){
			for (int l = 0; l < 3 * c_width; l++){
				if (!c_pole[k][l]){
					printf(" ");
				}
				else{
					printf("#");
				}
			}

			printf("     ");
			printf("%d", k % 10);

			for (int l = 0; l < 3 * c_width; l++){
				if (je_v_poli(c_obarveno[k][l], c_barvy_cykly, 0, c_barvy_cykly_pointer - 1)){
					//printf(" ");
					printf("%d", c_obarveno[k][l]);
				}
				else{
					c_obarveno[k][l] = -1;
					printf(" ");
				}
			}
			printf("\n");
		}
		printf("\n");
		*/
	}

	return 0;
}