#include "main.h"

int main(int argc, char *argv[]){
	if (argc == 2 && argv[1][0] == '0'){
		generator();
	}
	else if (argc == 2 && argv[1][0] == '1'){
		//main_no_paralel(); 
		//je nutne pouzit max 7000 v const.h aby program bezel po odkomentovani nebo zakomentovat jiny soubor
		//take je treba odkomentovat include v main.h
	}
	else if (argc == 2 && argv[1][0] == '2'){
		main_no_recursive();
	}
	else if (argc == 2 && argv[1][0] == '3'){
		main_no_rec_diff_colors();
	}
	else if (argc == 2 && argv[1][0] == '4'){
		main_paralel();
	}
	else if (argc == 2 && argv[1][0] == '5'){
		main_like_paralel();
	}
	else{
		printf("\nProgram je spousten s 1 parametrem:\n");
		printf("0 - generovani vstupu\n");
		printf("1 - spravny rekurzivni alg po odkomentovani\n");
		printf("2 - nerekurzivni alg\n");
		printf("3 - nerekurzivni alg s ruznymi barvami pro vyplnene oblasti\n");
		printf("4 - jako paralelni\n");
		printf("5 - paralelni\n");
		printf(" - \n");
		printf(" - \n");
		printf(" - \n");
		//generator();
		//main_no_paralel();
		//main_no_recursive();
		//main_no_rec_diff_colors();
		//main_like_paralel();
		//main_paralel();
	}
	
}