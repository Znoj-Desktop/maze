#ifndef _STACK_H
#define _STACK_H

#include <stdio.h>
#include "const.h"


bool s_pop(int &x, int &y, int h);
bool s_push(int x, int y, int h);
void s_emptyStack(int h);


#endif