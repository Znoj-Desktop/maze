#include "paralel.h"


const int pocet_vlaken = 4; //zde lze zm�nit po�et vl�ken
#define p_stackSize 16777216
int p_stack[pocet_vlaken][p_stackSize];

//globalni promenne vyuzivane v alg
int p_width, p_height;
bool p_pole[MAX][MAX];
unsigned int p_obarveno[MAX][MAX];

const int max_pole = (MAX / 3)*(MAX / 3);
unsigned int barvy_cykly[max_pole][2];
int barvy_cykly_pointer = 0;


bool p_pop(int &x, int &y, int h, int p_stack[p_stackSize], int &p_stackPointer)
{
	if (p_stackPointer > 0)
	{
		int p = p_stack[p_stackPointer];
		x = p / h;
		y = p % h;
		p_stackPointer--;
		return 1;
	}
	else
	{
		return 0;
	}
}

bool p_push(int x, int y, int h, int p_stack[p_stackSize], int &p_stackPointer)
{
	if (p_stackPointer < p_stackSize - 1)
	{
		p_stackPointer++;
		p_stack[p_stackPointer] = h * x + y;
		return 1;
	}
	else
	{
		return 0;
	}
}

void p_emptyStack(int h, int p_stack[p_stackSize], int &p_stackPointer)
{
	//int x, y;
	//while (lp_pop(x, y, h, lp_stack, lp_stackPointer));
	p_stackPointer = 0;
}



//lehce modifikovane seminkove vyplnovani
//Otestuj zda bod ji� nebyl vypln�n nebo nele�� na hranici.
//Pokud ano skon��, pokud ne vypl� tento bod a ur�i v�echny sousedn� body.
//Pro v�echny sousedn� body znovu pou�ij tento algoritmus.

void vypis();



int seminkove_paralel(int x, int y, int hl, int hh, int p_stack[p_stackSize], int &p_stackPointer, unsigned int &p_barva){
	//pokud jsem mimo hranice, pak vracim -1,
	//ktera znaci, ze se nejedna o kruznici
	if (x >= 3 * p_width || x < 0 || y < hl || y >= hh){
		return -1;
	}
	//pokud narazim na jiz obarveny uzel nebo okraj kruznice, tak vracim 0
	else if ((p_obarveno[y][x] > 0) || p_pole[y][x]){
		return 0;
	}
	else{
		int vpravo = 0;
		int vlevo = 0;
		int nahore = 0;
		int dole = 0;
		int pocet = 0;
		bool cyklus = 1;
		//?? fakt?
		p_emptyStack(3 * p_height, p_stack, p_stackPointer);
		if (!p_push(x, y, 3 * p_height, p_stack, p_stackPointer)){
			return -2;
		}
		while (p_pop(x, y, 3 * p_height, p_stack, p_stackPointer)){
			//printf("SP - vlakno: %d, %d, %d, %d, %d\n", omp_get_thread_num(), x, y, p_stackPointer, p_stack[0]);
			if (p_obarveno[y][x] > 0){
				continue;
			}
			pocet++;
			p_obarveno[y][x] = p_barva;

			/*
			#pragma omp sections
			{
				#pragma omp section
				{
					vypis();
				}
			}
			*/
			//jdu se podivat do vsech smeru
			if ((x + 1) >= 3 * p_width){
				vpravo = -1;
				cyklus = 0;
			}
			else if ((p_obarveno[y][x + 1] == 0) && !p_pole[y][x + 1]){
				if (!p_push(x + 1, y, 3 * p_height, p_stack, p_stackPointer)) {
					return -2;
				}
				//vpravo += 1;
			}

			if ((x - 1) < 0){
				vlevo = -1;
				cyklus = 0;
			}
			else if (p_obarveno[y][x - 1] == 0 && !p_pole[y][x - 1]){
				if (!p_push(x - 1, y, 3 * p_height, p_stack, p_stackPointer)){
					return -2;
				}
				//vlevo += 1;
			}

			if ((y + 1) >= 3 * p_height){
				nahore = -1;
				cyklus = 0;
			}
			//mimo aktualni vysec
			else if ((y + 1) >= hh){
				;
			}
			else if (p_obarveno[y + 1][x] == 0 && !p_pole[y + 1][x]){
				if (!p_push(x, y + 1, 3 * p_height, p_stack, p_stackPointer)){
					return -2;
				}
				//nahore += 1;
			}
			if ((y - 1) < 0){
				dole = -1;
				cyklus = 0;
			}
			//mimo aktualni vysec
			if ((y - 1) < hl){
				;
			}
			else if (p_obarveno[y - 1][x] == 0 && !p_pole[y - 1][x]){
				if (!p_push(x, y - 1, 3 * p_height, p_stack, p_stackPointer)){
					return -2;
				}
				//dole += 1;
			}
		} //konec while

		/*

		vypis();

		*/


		//pokud nejsem mimo p_pole, pricitam
		if (cyklus && vpravo >= 0 && vlevo >= 0 && nahore >= 0 && dole >= 0){
			//return (vpravo + vlevo + nahore + dole + 1);
			return pocet + 1;
		}
		else{
			//znacim ze se nejedna o kruznici
			return -1;
			//return pocet + 1;
		}
	}
} //konec seminkoveho vyplnovani

/*
int je_v_poli_p(int prvek, int pole[][2], int min, int max){
	if (prvek == 0){
		return -1;
	}
	int index = ((max - min) / 2) + min;
	if (pole[index][0] == prvek){
		return index;
	}
	else if (index == min){
		if (pole[max][0] == prvek){
			return index;
		}
		return -1;
	}
	else if (prvek < pole[index][0]){
		return je_v_poli_p(prvek, pole, min, index);
	}
	else{
		return je_v_poli_p(prvek, pole, index, max);
	}
	return -1;
}
*/
int je_v_poli_p(int prvek, unsigned int pole[][2], int min, int max){
	//printf("prvek: %d\n", prvek);
	for (int i = min; i <= max; i++){
		//printf("%d\n", pole[i][0]);
		if (prvek == pole[i][0]){
			return i;
		}
	}
	return -1;
}

void zpracuj(int co, int na_co){
	for (int i = 0; i < 3 * p_height; i++){
		for (int j = 0; j < 3 * p_width; j++){
			if (p_obarveno[i][j] == co){
				p_obarveno[i][j] = na_co;
			}
		}
	}
}

void vypis(){
	for (int l = 0; l < 3 * p_width; l++){
		printf("%d", l % 10);
	}
	printf("     X");
	for (int l = 0; l < 3 * p_width; l++){
		printf("%d", l % 10);
	}
	printf("\n");

	for (int k = 3 * p_height - 1; k >= 0; k--){
		for (int l = 0; l < 3 * p_width; l++){
			if (!p_pole[k][l]){
				printf("  ");
			}
			else{
				printf("##");
			}
		}

		printf("     ");
		printf("%d", k % 10);

		for (int l = 0; l < 3 * p_width; l++){
			if (p_obarveno[k][l] > 0){
				printf("%2d", p_obarveno[k][l]);
				//printf("*");
			}
			else{
				printf("  ");
			}
		}
		printf("\n");
	}
	printf("\n");
}

void vypis_final(){
	for (int l = 0; l < 3 * p_width; l++){
		printf("%d", l % 10);
	}
	printf("     X");
	for (int l = 0; l < 3 * p_width; l++){
		printf("%d", l % 10);
	}
	printf("\n");

	for (int k = 3 * p_height - 1; k >= 0; k--){
		for (int l = 0; l < 3 * p_width; l++){
			if (!p_pole[k][l]){
				printf(" ");
			}
			else{
				printf("#");
			}
		}

		printf("     ");
		printf("%d", k % 10);

		for (int l = 0; l < 3 * p_width; l++){
			if (je_v_poli_p(p_obarveno[k][l], barvy_cykly, 0, barvy_cykly_pointer - 1) >= 0){
				//printf("%d", (p_obarveno[k][l]));
				//printf("*");
				printf(" ");
			}
			else{
				p_obarveno[k][l] = -1;
				printf("#");
				//printf(" ");
			}
		}
		printf("\n");
	}
}

int main_paralel(){
	int poradi = 1;
	unsigned int nejdelsi;
	int kruznic;
	char znak;
	int i;
	int j;
	clock_t start, finish;

	//nekonecny cyklus pro nacitani vstupu
	while (scanf_s("%i %i", &p_width, &p_height) > 0){
		//zde je konec programu--------------------------
		if (p_width == 0 && p_height == 0){
			return 0;
		}

		//nulovani
		nejdelsi = 0;
		kruznic = 0;

		//nulovani poli - jen pro jistotu...
#pragma omp parallel for
		for (i = 0; i < MAX; i++){
			for (j = 0; j < MAX; j++){
				p_pole[i][j] = 0;
				p_obarveno[i][j] = 0;
			}
		}

		//ulozeni vstupu do p_pole
		for (i = 0; i < p_height; i++){
			for (j = 0; j < p_width; j++){
				znak = getchar();
				//preskoceni nepatricnych znaku
				while (znak != '/' && znak != '\\'){
					znak = getchar();
				}
				//zakresleni znaku '/' do ctverce 3x3
				if (znak == '/'){
					p_pole[3 * i + 0][3 * j + 2] = true;
					p_pole[3 * i + 2][3 * j + 0] = true;
				}
				//zakresleni znaku '\' do ctverce 3x3
				else{
					p_pole[3 * i + 0][3 * j + 0] = true;
					p_pole[3 * i + 2][3 * j + 2] = true;
				}
				//pro '/' i '\' je prostredni ctverec stejny
				p_pole[3 * i + 1][3 * j + 1] = true;
			}
		}
		int velikost_kruznice;
		printf("Po�et vl�ken: %d\n", pocet_vlaken);
		start = clock();
		//----------------------------------------------------------------
		//[pocet vlaken][min_h, max_h]

		int rozmery[pocet_vlaken][2];

		rozmery[0][0] = 0;
		rozmery[0][1] = p_height / pocet_vlaken;

#pragma omp parallel for
		for (int i = 1; i < 4; i++){
			rozmery[i][0] = rozmery[i - 1][1];
			rozmery[i][1] = (i + 1) * rozmery[0][1];
		}
		rozmery[3][1] = p_height;

		int p_stackPointer = 0;
		int vlakno = 0;
		unsigned int global_p_barva = 2;	//cislo 1 znamena, ze zde byla barva
		i = 0;
		j = 0;
		int r = 0;

#pragma omp parallel for private(i, j, velikost_kruznice, global_p_barva, p_stackPointer), shared(r, pocet_vlaken, barvy_cykly, barvy_cykly_pointer, p_stack), reduction(+:kruznic), ordered
		//
		//#pragma omp parallel sections private(i, j, velikost_kruznice, p_stackPointer, global_p_barva), shared(r, barvy_cykly, barvy_cykly_pointer), reduction(+:kruznic) 
		//		{
		//#pragma omp section
		//			{
		//				r = 0;
		for (r = 0; r < pocet_vlaken; r++){
			//menim barvu
			global_p_barva = r*(16777214 / pocet_vlaken) + 2;
			p_stackPointer = 0;
			for (i = 3 * rozmery[r][0]; i < 3 * rozmery[r][1]; i++){
				//printf("i - vlakno: %d\n", omp_get_thread_num());
				for (j = 0; j < 3 * p_width; j++){
					//printf("i: %d, j: %d\n", i, j);
					//printf("j - vlakno: %d\n", omp_get_thread_num());
					#pragma omp ordered
					{
						velikost_kruznice = seminkove_paralel(j, i, 3 * rozmery[r][0], 3 * rozmery[r][1], p_stack[r], p_stackPointer, global_p_barva);
					}

					if (velikost_kruznice == 0){ //bod je jiz obarven
						continue;
					}
					else{
						//v pripade ze byla barva pouzita, tak barvu menim
						//if (p_stackPointer == 0){
							global_p_barva++;
						//}
					}
					if (velikost_kruznice == -2){
						printf("\npodteceni nebo preteceni zasobniku: %d\n", r);
						continue;
					}
					if (velikost_kruznice > 0){
						//barva je zmenena - zapisuju ale tu, kterou byl delan cyklus
						//printf("barva: %d\n", global_p_barva - 1);
						barvy_cykly[barvy_cykly_pointer][0] = global_p_barva - 1;
						barvy_cykly[barvy_cykly_pointer][1] = velikost_kruznice / 3;
						barvy_cykly_pointer++;
						kruznic++;
					}
				}
			}
		}

		//vypis();
		//printf("BarvyCyklyPointer: %d\n", barvy_cykly_pointer);
		double middle = ((double)(clock() - start)) / CLOCKS_PER_SEC;
		//----------------------------------------------------------------
		//postprocessing
		printf("");
		for (int r = 0; r < (pocet_vlaken - 1); r++){
			for (int j = 0; j < 3 * p_width; j++){
				//pokud jsou cisla na hranici ruzna a znaci barvy { hranice: 3 * rozmery[r][1], (3 * rozmery[r][1]) + 1 }
				if ((p_obarveno[(3 * rozmery[r][1]) - 1][j] > 0) && (p_obarveno[(3 * rozmery[r][1])][j] > 0)){
					if (((p_obarveno[(3 * rozmery[r][1]) - 1][j]) != (p_obarveno[(3 * rozmery[r][1])][j]))){
						//jedna se o cyklus
						int prvek1 = je_v_poli_p(p_obarveno[(3 * rozmery[r][1]) - 1][j], barvy_cykly, 0, barvy_cykly_pointer - 1);
						int prvek2 = je_v_poli_p(p_obarveno[(3 * rozmery[r][1])][j], barvy_cykly, 0, barvy_cykly_pointer - 1);
						if ((prvek1 >= 0) && (prvek2 >= 0)){
							//co se zobrazi na co
							zpracuj(p_obarveno[(3 * rozmery[r][1])][j], p_obarveno[(3 * rozmery[r][1]) - 1][j]);
							barvy_cykly[prvek1][1] += barvy_cykly[prvek2][1];
							barvy_cykly[prvek2][1] = 0;
							kruznic--;
						}
						//nejaka cast se dotyka okraje - nejde o cyklus
						else{
							if (prvek1 >= 0){
								barvy_cykly[prvek1][1] = 0;
								kruznic--;
							}
							else if (prvek2 >= 0){
								barvy_cykly[prvek2][1] = 0;
								kruznic--;
							}
							zpracuj(p_obarveno[(3 * rozmery[r][1]) - 1][j], 1);
							zpracuj(p_obarveno[(3 * rozmery[r][1])][j], 1);
						}
					}
				}


				//vypis();


			}
		}
		//-----------------------------------------------------------------
		finish = clock();
		//finalni vypis
		printf("Maze #%d:\n", poradi++);

		if (kruznic == 0){
			printf("There are no cycles.\n\n");
		}
		else{
			nejdelsi = barvy_cykly[0][1];
			for (int i = 1; i < barvy_cykly_pointer; i++){
				if (barvy_cykly[i][1] > nejdelsi){
					nejdelsi = barvy_cykly[i][1];
				}
			}
			printf("%d Cycles; the longest has length %d.\n\n", kruznic, nejdelsi);
		}
		printf("It middle: %fs\n", middle);
		printf("It took %fs\n", ((double)(finish - start)) / CLOCKS_PER_SEC);

		/*
		for (int i = 0; i < barvy_cykly_pointer; i++){
		printf("%d - %d  ", i, barvy_cykly[i]);
		}
		printf("\n\n");*/
		//----------------------------------------------------------------

		//vypis_final();

		//----------------------------------------------------------------

		/*
		printf("Number of procesors: %d\n", omp_get_num_procs());
#pragma omp parallel num_threads(4)
		{
#pragma omp for //paralelizuje for
			for (int i = 0; i < 10; i++){
				printf("thread: %d; number: %d\n", omp_get_thread_num(), i);
			}
		}
		*/
	}
	return 0;
}