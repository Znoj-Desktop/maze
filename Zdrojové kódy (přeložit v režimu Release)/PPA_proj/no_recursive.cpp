#include "no_recursive.h"

#define stackSize 16777216
int stack[stackSize];
int stackPointer;

bool pop(int &x, int &y, int h)
{
	if (stackPointer > 0)
	{
		int p = stack[stackPointer];
		x = p / h;
		y = p % h;
		stackPointer--;
		return 1;
	}
	else
	{
		return 0;
	}
}

bool push(int x, int y, int h)
{
	if (stackPointer < stackSize - 1)
	{
		stackPointer++;
		stack[stackPointer] = h * x + y;
		return 1;
	}
	else
	{
		return 0;
	}
}

void emptyStack(int h)
{
	int x, y;
	while (pop(x, y, h));
}




//globalni promenne vyuzivane v alg
int width, height;
bool n_pole[MAX][MAX];
bool n_obarveno[MAX][MAX];

//lehce modifikovane seminkove vyplnovani
//Otestuj zda bod ji� nebyl vypln�n nebo nele�� na hranici.
//Pokud ano skon��, pokud ne vypl� tento bod a ur�i v�echny sousedn� body.
//Pro v�echny sousedn� body znovu pou�ij tento algoritmus.
int seminkove_no(int x, int y){
	//pokud jsem mimo hranice, pak vracim -1,
	//ktera znaci, ze se nejedna o kruznici
	if (x >= 3 * width || x < 0 || y < 0 || y >= 3 * height){
		return -1;
	}
	//pokud narazim na jiz obarveny uzel nebo okraj kruznice, tak vracim 0
	else if (n_obarveno[y][x] || n_pole[y][x]){
		return 0;
	}
	else{
		int vpravo = 0;
		int vlevo = 0;
		int nahore = 0;
		int dole = 0;
		int pocet = 0;
		bool cyklus = 1;
		//?? fakt?
		emptyStack(3 * height);
		if (!push(x, y, 3 * height)){
			return -2;
		}
		while (pop(x, y, 3 * height)){
			if (n_obarveno[y][x]){
				continue;
			}
			pocet++;
			n_obarveno[y][x] = true;
			/*
			for (int l = 0; l < 3 * width; l++){
				printf("%d", l % 10);
			}
			printf("     X");
			for (int l = 0; l < 3 * width; l++){
				printf("%d", l % 10);
			}
			printf("\n");
			for (int k = 3 * height - 1; k >= 0; k--){
				for (int l = 0; l < 3 * width; l++){
					if (!n_pole[k][l]){
						printf(" ");
					}
					else{
						printf("#");
					}
				}

				printf("     ");
				printf("%d", k % 10);
				
				for (int l = 0; l < 3 * width; l++){
					if (n_obarveno[k][l]){
						printf(" ");
					}
					else{
						printf("#");
					}
				}
				printf("\n");
			}
			printf("\n");
			*/
			//jdu se podivat do vsech smeru
			if ((x + 1) >= 3 * width){
				vpravo = -1;
				cyklus = 0;
			}
			else if (!n_obarveno[y][x + 1] && !n_pole[y][x + 1]){
				if (!push(x + 1, y, 3 * height)) {
					return -2;
				}
				//vpravo += 1;
			}

			if ((x - 1) < 0){
				vlevo = -1;
				cyklus = 0;
			}
			else if (!n_obarveno[y][x - 1] && !n_pole[y][x - 1]){
				if (!push(x - 1, y, 3 * height)){
					 return -2;
				}
				//vlevo += 1;
			}

			if ((y + 1) >= 3 * height){
				nahore = -1;
				cyklus = 0;
			}
			else if (!n_obarveno[y + 1][x] && !n_pole[y + 1][x]){
				if (!push(x, y + 1, 3 * height)){
					return -2;
				}
				//nahore += 1;
			}

			if ((y - 1) < 0){
				dole = -1;
				cyklus = 0;
			}
			else if (!n_obarveno[y - 1][x] && !n_pole[y - 1][x]){
				if (!push(x, y - 1, 3 * height)){
					return -2;
				}
				//dole += 1;
			}
		} //konec while
		//pokud nejsem mimo n_pole, pricitam
		if (cyklus && vpravo >= 0 && vlevo >= 0 && nahore >= 0 && dole >= 0){
			//return (vpravo + vlevo + nahore + dole + 1);
			return pocet + 1;
		}
		else{
			//znacim ze se nejedna o kruznici
			return -1;
		}
	}
} //konec seminkoveho vyplnovani


int main_no_recursive(){
	int poradi = 1;
	int nejdelsi;
	int kruznic;
	char znak;
	int i;
	int j;
	clock_t start, finish;

	//nekonecny cyklus pro nacitani vstupu
	while (scanf_s("%i %i", &width, &height) > 0){
		//zde je konec programu--------------------------
		if (width == 0 && height == 0){
			return 0;
		}

		//nulovani
		nejdelsi = 0;
		kruznic = 0;

		//nulovani poli - jen pro jistotu...
		for (i = 0; i < MAX; i++){
			for (j = 0; j < MAX; j++){
				n_pole[i][j] = 0;
				n_obarveno[i][j] = 0;
			}
		}

		//ulozeni vstupu do n_pole
		for (i = 0; i < height; i++){
			for (j = 0; j < width; j++){
				znak = getchar();
				//preskoceni nepatricnych znaku
				while (znak != '/' && znak != '\\'){
					znak = getchar();
				}
				//zakresleni znaku '/' do ctverce 3x3
				if (znak == '/'){
					n_pole[3 * i + 0][3 * j + 2] = true;
					n_pole[3 * i + 2][3 * j + 0] = true;
				}
				//zakresleni znaku '\' do ctverce 3x3
				else{
					n_pole[3 * i + 0][3 * j + 0] = true;
					n_pole[3 * i + 2][3 * j + 2] = true;
				}
				//pro '/' i '\' je prostredni ctverec stejny
				n_pole[3 * i + 1][3 * j + 1] = true;
			}
		}
		int velikost_kruznice;
		start = clock();
		//pro kazdy znak zavolej funkci seminkove_no
		for (i = 0; i < 3 * height; i++){
			for (j = 0; j < 3 * width; j++){
				velikost_kruznice = seminkove_no(j, i);
				if (velikost_kruznice == -2){
					printf("\npodteceni nebo preteceni zasobniku.\n");
					break;
				}
				//je potreba velikost vydelit 3
				velikost_kruznice /= 3;
				if (velikost_kruznice > 0){
					if (velikost_kruznice > nejdelsi){
						nejdelsi = velikost_kruznice;
					}
					kruznic++;
				}
			}
		}
		finish = clock();
		//finalni vypis
		printf("Maze #%d:\n", poradi++);

		if (kruznic == 0){
			printf("There are no cycles.\n\n");
		}
		else{
			printf("%d Cycles; the longest has length %d.\n\n", kruznic, nejdelsi);
		}
		printf("It took %fs", ((double)(finish - start))/CLOCKS_PER_SEC);
		printf("\n");
		/*
		for (int l = 0; l < 3 * width; l++){
				printf("%d", l % 10);
			}
			printf("     X");
			for (int l = 0; l < 3 * width; l++){
				printf("%d", l % 10);
			}
			printf("\n");
			for (int k = 3 * height - 1; k >= 0; k--){
				for (int l = 0; l < 3 * width; l++){
					if (!n_pole[k][l]){
						printf(" ");
					}
					else{
						printf("#");
					}
				}

				printf("     ");
				printf("%d", k % 10);
				
				for (int l = 0; l < 3 * width; l++){
					if (n_obarveno[k][l]){
						printf(" ");
					}
					else{
						printf("#");
					}
				}
				printf("\n");
			}
			printf("\n");
	*/
	}
	return 0;
}